# -*- coding: utf-8 -*-
{
    "name": "bc_reserve",
    "author": "xiaonibaba",
    "version": "0.1",
    "depends": ["base", 'bc_system', 'bc_basic'],
    "description": """《buynow商务中心》预定""",
    "init_xml": [],
    "update_xml": ['views/bc_reserve.xml', 'workflow/bc_reserve.xml', 'views/link.xml',
                   'views/bc_reserve_summary.xml', 'menu.xml'],

    "qweb": ['static/src/xml/room_summary.xml'],
    "installable": True,
}
