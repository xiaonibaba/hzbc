# -*- coding: utf-8 -*-
from openerp import models, fields, api, _

class bc_contract_expense(models.Model):
    _name = 'bc.contract.expense'

    contract_id = fields.Many2one('bc.contract')
    expenseitem_id = fields.Many2one('bc.expenseitem')
    price = fields.Float()
