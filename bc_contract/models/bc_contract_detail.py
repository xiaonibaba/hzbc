# -*- coding: utf-8 -*-

from date_help import date_help
from openerp import models, fields, api, _


class bc_contract_detail(models.Model):
    _name = 'bc.contract.detail'

    contract_id = fields.Many2one('bc.contract', ondelete='cascade', required=True)
    resource_id = fields.Many2one('bc.resource')
    resouretype_id = fields.Many2one('bc.resouretype')
    expenseitem_id = fields.Many2one('bc.expenseitem')
    price = fields.Float()
    state = fields.Selection([('draft', u'草稿'), ('audit', u'审核'), ('rent', u'退租'), ('close', u'关闭'), ],
                             related='contract_id.state', store=True)
    startdate = fields.Date(related='contract_id.startdate', store=True)
    enddate = fields.Date(related='contract_id.enddate', store=True)
    unit = fields.Selection([('month', u'/每月'), ('week', u'/每周'), ('day', u'/每日'), ('every', u'/次')])
    interval = fields.Integer()
    unit2 = fields.Selection([('month', u'/月收取一次'), ('week', u'/周收取一次'), ('day', u'/日收取一次'), ('every', u'/次收取一次')])
    total = fields.Float(compute='_compute_total', store=True)
    contract_period_id = fields.One2many('bc.contract.period', 'contract_detail_id')

    @api.one
    @api.constrains('unit', 'unit2')
    def _check_date(self):
        if self.unit != self.unit2:
            raise ValueError(_("unit和unit2,必须一致!"))

    @api.one
    @api.constrains('resouretype_id', 'resource_id')
    def _check_date(self):
        if self.resouretype_id.id != self.resource_id.resourcetype.id:
            raise ValueError(_("resouretype必须一致!"))

    @api.one
    @api.onchange('resouretype_id')
    def _onchange_resouretype(self):
        if self.resouretype_id:
            self.resource_id = False
            self.expenseitem_id = False
            self.price = 0
            self.unit = False
            self.interval = 0
            self.unit2 = False

    @api.one
    @api.onchange('resource_id', 'unit')
    def _onchange_resoure(self):
        if self.resource_id.id and self.unit:
            self.resouretype_id = self.resource_id.resourcetype.id
            obj_Prc = self.env['bc.resouretype.price'].search(
                [('resouretype_id', '=', self.resouretype_id.id), ('unit', '=', self.unit)])
            if not self.price:
                self.price = obj_Prc.price

    @api.one
    @api.onchange('unit', 'unit2')
    def _onchange_unit(self):
        if self.unit:
            self.unit2 = self.unit
            if self.unit == 'every':
                self.interval = 1
            elif self.unit == 'month':
                self.interval = 6
            elif self.unit == 'week':
                self.interval = 6
            elif self.unit == 'day':
                self.interval = 7

    @api.one
    @api.depends('price', 'startdate', 'enddate')
    def _compute_total(self):
        if self.startdate and self.price:
            diff_day = date_help.get_daysvalue(self.startdate, self.enddate)
            diff_month = date_help.get_monthvalue(self.startdate, self.enddate)
            if self.unit == 'month':
                self.total = diff_month * self.price
            elif self.unit == 'week':
                self.total = diff_day * self.price / 7.0
            elif self.unit == 'day':
                self.total = diff_day * self.price
            elif self.unit == 'every':
                self.total = self.price

    def get_state(self, resource_id, date):
        list = [obj.state for obj in self.search([
            ('resource_id', '=', resource_id), ('enddate', '>=', date), ('startdate', '<=', date)])]

        if list:
            return list[0]

        return 'Free'
