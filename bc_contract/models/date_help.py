# -*- coding: utf-8 -*-
import datetime

from dateutil.relativedelta import relativedelta


class date_help(object):
    @staticmethod
    def str_to_date(str, formate=None):
        if not formate:
            formate = "%Y-%m-%d"
        return datetime.datetime.strptime(str, formate)

    @staticmethod
    def add_day(dtdates, nday):
        dates = date_help.str_to_date(dtdates)
        delta = datetime.timedelta(days=nday)
        return (dates + delta).strftime('%Y-%m-%d')

    # 取给定的日期,当月第一天和最后一天
    @staticmethod
    def getmonth_firstday_and_endday(dtdates):
        d = date_help.str_to_date(dtdates)
        year = d.year
        month = d.month

        firstday = datetime.datetime(year, month, 1).strftime('%Y-%m-%d')
        endday = datetime.datetime(year, month, 1) + relativedelta(months=1) - relativedelta(days=1)
        endday = endday.strftime('%Y-%m-%d')
        return [firstday, endday]

    # 添加月份
    @staticmethod
    def add_months(dtdates, months):
        if type(dtdates) == type('a'):
            trn_date = date_help.str_to_date(dtdates) + relativedelta(months=months)
        else:
            trn_date = dtdates + relativedelta(months=months)

        trn_date = trn_date.strftime('%Y-%m-%d')
        return trn_date

    @staticmethod
    def subtraction_months(dtbegindate, dtenddate):
        dtbeginyear = dtbegindate.year
        dtbeginmonth = dtbegindate.month
        dtendyear = dtenddate.year
        dtendmonth = dtenddate.month
        months = 0
        if dtendyear - dtbeginyear > 0:
            months = (dtendyear - dtbeginyear) * 12
        months = months - dtbeginmonth + dtendmonth
        return months

    # 如2016-1-5 2016-2-28 则 1 + (28-4)/29
    @staticmethod
    def get_monthvalue(dtbegindate, dtenddate):
        d1 = date_help.str_to_date(dtbegindate)
        d2 = date_help.str_to_date(dtenddate)
        if d2.month + 1 > 12:
            dtCurMonthEndDate = datetime.date(d2.year + 1, 1, 1) - datetime.timedelta(1)
        else:
            dtCurMonthEndDate = datetime.date(d2.year, d2.month + 1, 1) - datetime.timedelta(1)

        months = date_help.subtraction_months(d1, d2)
        dtnewdate = date_help.add_months(d1, months)
        if date_help.add_day(dtnewdate, -1) == dtenddate:
            mresult = months
        elif dtenddate > dtnewdate:
            mresult = float((d2 - date_help.str_to_date(dtnewdate)).days + 1) / (dtCurMonthEndDate.day)
            mresult = months + mresult
        else:
            mresult = float((date_help.str_to_date(dtnewdate) - d2).days + 1) / (dtCurMonthEndDate.day)
            mresult = months - mresult
        return mresult

    @staticmethod
    def get_daysvalue(dtbegindate, dtenddate):
        d1 = date_help.str_to_date(dtbegindate)
        d2 = date_help.str_to_date(dtenddate)
        return (d2 - d1).days + 1

    @staticmethod
    def get_moeny(unit, price, startdate, enddate):
        """
        返回金额
        :param unit:
        :param price:
        :param startdate:
        :param enddate:
        :return:
        """
        diff_day = date_help.get_daysvalue(startdate, enddate)
        diff_month = date_help.get_monthvalue(startdate, enddate)
        total = 0
        if unit == 'month':
            total = diff_month * price
        elif unit == 'week':
            total = diff_day * price / 7.0
        elif unit == 'day':
            total = diff_day * price
        elif unit == 'every':
            total = price
        return total

    @staticmethod
    def get_enddate(unit, interval, startdate, enddate):
        """
        返回结束时间
        :param unit:
        :param interval:
        :param startdate:
        :param enddate:
        :return:
        """
        if unit == 'month':
            _enddate = date_help.str_to_date(date_help.add_months(startdate, interval))
        elif unit == 'week':
            _enddate = startdate + datetime.timedelta(days=7 * interval)
        elif unit == 'day':
            _enddate = startdate + datetime.timedelta(days=interval)

        _enddate += datetime.timedelta(days=-1)

        if _enddate > enddate:
            _enddate = enddate

        return _enddate
