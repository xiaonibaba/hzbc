# -*- coding: utf-8 -*-
{
    "name": "bc_contract",
    "author": "xiaonibaba",
    "version": "0.1",
    "depends": ["base",'bc_system','bc_basic','bc_reserve'],
    "description": """《buynow商务中心》入住管理""",
    "init_xml": [],
    "update_xml": ['views/bc_contract.xml','workflow/bc_contract.xml','menu.xml',],
    "installable": True,
}

