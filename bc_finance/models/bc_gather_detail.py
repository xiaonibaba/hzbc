# -*- coding: utf-8 -*-
from openerp import models, fields, api, _


class bc_gather_detail(models.Model):
    _name = 'bc.gather.detail'

    gathermaster_id = fields.Many2one('bc.gathermaster', ondelete='cascade')
    planbill_detail_id = fields.Many2one('bc.planbill.detail', ondelete='cascade')
    bussines_id = fields.Many2one('bc.bussines', related='planbill_detail_id.bussines_id')
    resource_id = fields.Many2one('bc.resource', related='planbill_detail_id.resource_id')
    expenseitem_id = fields.Many2one('bc.expenseitem', related='planbill_detail_id.expenseitem_id')
    startdate = fields.Date(related='planbill_detail_id.startdate')
    enddate = fields.Date(related='planbill_detail_id.enddate')
    decmoney = fields.Float(related='planbill_detail_id.decmoney')
    sparemoney = fields.Float()
    hxdecmoney = fields.Float()
    remark = fields.Char()

    def get_bussines_hx_moeny(self, bussines_id):
        """
        获取客户所有交的钱
        :param bussines_id:
        :return:float
        """
        return sum(gatherdetail.hxdecmoney for gatherdetail in self.search([('bussines_id', '=', bussines_id)]))

    @api.multi
    def unlink(self):
        if self.state in ('audit', 'close'):
            raise Warning(_(u'状态是{0},不能删除'.format(self.state)))
        return super(bc_gather_detail, self).unlink()
