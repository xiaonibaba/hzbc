# -*- coding: utf-8 -*-
{
    "name": "bc_finance",
    "author": "xiaonibaba",
    "version": "0.1",
    "depends": ["base", 'bc_system', 'bc_basic', 'bc_reserve', 'bc_contract'],
    "description": """《buynow商务中心》财务管理""",
    "init_xml": [],
    "update_xml": ['views/bc_planbill.xml', 'workflow/bc_planbill.xml', 'views/bc_payment.xml',
                   'workflow/bc_payment.xml', 'views/bc_gathermaster.xml',
                   'workflow/bc_gathermaster.xml', 'menu.xml', ],
    "installable": True,
}
