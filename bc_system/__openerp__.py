# -*- coding: utf-8 -*-
{
    "name": "bc_system",
    "author": "xiaonibaba",
    "version": "0.1",
    "depends": ["base"],
    "description": """《buynow商务中心》系统管理""",
    "init_xml": [],
    "update_xml": ['views/bc_enumparent.xml', 'views/bc_enum.xml', 'views/bc_bussinesstype.xml',
                   'views/bc_resouretype.xml', 'views/bc_expenseitem.xml', 'menu.xml',
                   'workflow/bc_bussinesstype.xml', 'workflow/bc_enum.xml','workflow/bc_enumparent.xml',
                   'workflow/bc_expenseitem.xml','workflow/bc_resouretype.xml'],
    "installable": True,
}
