# -*- coding: utf-8 -*-
import datetime
from openerp import models, fields, api, _ 
from openerp.exceptions import Warning

class bc_resouretype_price(models.Model):
    _name = 'bc.resouretype.price'

    resouretype_id = fields.Many2one('bc.resouretype', )
    unit = fields.Selection([('month', u'/每月'), ('week', u'/每周'), ('day', u'/每日'), ('every', u'/次')])
    price = fields.Float()
    state = fields.Selection([('draft', u'草稿'), ('audit', u'审核'), ('close', u'关闭'), ('cancel', u'作废')])


    @api.model
    def create(self, vals):
        vals['state'] = 'draft'
        return super(bc_resouretype_price, self).create(vals)

    # 僅能刪除草稿狀態的資料
    @api.multi
    def unlink(self):
       if self.state not in ('draft') :
          raise Warning(_('state {0} can not be delete'.format(self.state)))
       return super(bc_resouretype_price,self).unlink()    

    # 审核
    @api.model
    def btn_audit(self):
        self.write({'state': 'audit', 'audit_uid': self.env.user.id, 'audit_date': datetime.datetime.utcnow()})
        return True

    # 反审核
    @api.model
    def btn_unaudit(self):
        self.write({'state': 'draft', 'audit_uid': False, 'audit_date': False})
        return True