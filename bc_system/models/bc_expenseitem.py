﻿# -*- coding: utf-8 -*-
import datetime
from openerp import models, fields, api, _ 
from openerp.exceptions import Warning

class bc_expenseitem(models.Model):
    _name = 'bc.expenseitem'

    name = fields.Char(required=True)
    remark = fields.Text()
    type = fields.Selection([('1', u'收入'), ('2', u'代收代付'), ], default='1')
    expense_Type = fields.Selection([('period', u'周期产生'), ('disposable', u'一次性费用'), ('deposit', u'押金')])
    posting = fields.Boolean()
    state = fields.Selection([('draft', u'草稿'), ('audit', u'审核'), ('close', u'关闭'), ('cancel', u'作废')])
    

    @api.model
    def create(self, vals):
        vals['state'] = 'draft'
        return super(bc_expenseitem, self).create(vals)
    
    # 僅能刪除草稿狀態的資料
    @api.multi
    def unlink(self):
       if self.state not in ('draft') :
          raise Warning(_('state {0} can not be delete'.format(self.state)))
       return super(bc_expenseitem,self).unlink()    

    # 审核
    @api.model
    def btn_audit(self):
        self.write({'state': 'audit', 'audit_uid': self.env.user.id, 'audit_date': datetime.datetime.utcnow()})
        return True

    # 反审核
    @api.model
    def btn_unaudit(self):
        self.write({'state': 'draft', 'audit_uid': False, 'audit_date': False})
        return True