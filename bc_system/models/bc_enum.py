﻿# -*- coding: utf-8 -*-
import datetime
from openerp import models, fields, api, _ 
from openerp.exceptions import Warning

class bc_enum(models.Model):
    _name = 'bc.enum'
    _description = u'枚举设置'

    parent_id = fields.Many2one('bc.enum', domain="[('parent_id','=',False)]", help=u'父类')
    name = fields.Char(required=True)
    value = fields.Integer(required=True)
    index = fields.Integer()
    remark = fields.Text()
    str1 = fields.Char()
    str2 = fields.Char()
    str3 = fields.Char()
    str4 = fields.Char()
    state = fields.Selection([('draft', u'草稿'), ('audit', u'审核'), ('close', u'关闭'), ('cancel', u'作废')])
    
    @api.model
    def create(self, vals):
        vals['state'] = 'draft'
        return super(bc_enum, self).create(vals)
    
    # 僅能刪除草稿狀態的資料
    @api.multi
    def unlink(self):
       if self.state not in ('draft') :
          raise Warning(_('state {0} can not be delete'.format(self.state)))
       return super(bc_enum,self).unlink()    

    # 审核
    @api.model
    def btn_audit(self):
        self.write({'state': 'audit', 'audit_uid': self.env.user.id, 'audit_date': datetime.datetime.utcnow()})
        return True

    # 反审核
    @api.model
    def btn_unaudit(self):
        self.write({'state': 'draft', 'audit_uid': False, 'audit_date': False})
        return True