﻿# -*- coding: utf-8 -*-
import datetime
from openerp import models, fields, api, _ 
from openerp.exceptions import Warning


class bc_bussinesstype(models.Model):
    _name = 'bc.bussinesstype'

    name = fields.Char()
    remark = fields.Text()
    state = fields.Selection([('draft', u'草稿'), ('audit', u'审核'), ('close', u'关闭'), ('cancel', u'作废')])

    @api.model
    def create(self, vals):
        vals['state'] = 'draft'
        return super(bc_bussinesstype, self).create(vals)

    # 审核
    @api.model
    def btn_audit(self):
        self.write({'state': 'audit', 'audit_uid': self.env.user.id, 'audit_date': datetime.datetime.utcnow()})
        return True

    # 反审核
    @api.model
    def btn_unaudit(self):
        self.write({'state': 'draft', 'audit_uid': False, 'audit_date': False})
        return True
    
    # 僅能刪除草稿狀態的資料
    @api.multi
    def unlink(self):
       if self.state not in ('draft') :
          raise Warning(_('state {0} can not be delete'.format(self.state)))
       return super(bc_bussinesstype,self).unlink()

   