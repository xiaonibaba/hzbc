# -*- coding: utf-8 -*-
{
    "name": "bc_basic",
    "author": "xiaonibaba",
    "version": "0.1",
    "depends": ["base",'bc_system'],
    "description": """《buynow商务中心》基础资料""",
    "init_xml": [],
    "update_xml": ['views/bc_resource.xml','workflow/bc_resource.xml', 'views/bc_bussines.xml','workflow/bc_bussines.xml','menu.xml',],
    "installable": True,
}

