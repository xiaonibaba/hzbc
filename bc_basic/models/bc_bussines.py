﻿# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import Warning
import datetime


class bc_bussines(models.Model):
    _name = 'bc.bussines'

    name = fields.Char()
    strbusinesscode = fields.Char()
    strshortname = fields.Char()
    strbusinesstypeid = fields.Many2one('bc.bussinesstype')
    strtaxno = fields.Char()
    companyid = fields.Many2one('res.company', default=lambda self: self.env.user.company_id)
    strlinkman = fields.Char()
    stridcard = fields.Char()
    strlinktel = fields.Char()
    postcode = fields.Char()
    straddress = fields.Char()
    audit_uid = fields.Many2one('res.users')
    audit_date = fields.Datetime()
    state = fields.Selection([('draft', u'草稿'), ('audit', u'审核'), ('close', u'关闭')])
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id)

    @api.model
    def create(self, vals):
        vals['strbusinesscode'] = self.env['ir.sequence'].get('bc.bussiness')
        vals['state'] = 'draft'
        return super(bc_bussines, self).create(vals)
    
    # 僅能刪除草稿狀態的資料
    @api.multi
    def unlink(self):
       if self.state not in ('draft') :
          raise Warning(_('state {0} can not be delete'.format(self.state)))
       return super(bc_bussines,self).unlink()    

    # 审核
    @api.model
    def btn_audit(self):
        self.write({'state': 'audit', 'audit_uid': self.env.user.id, 'audit_date': datetime.datetime.utcnow()})
        return True

    # 反审核
    @api.model
    def btn_unaudit(self):
        self.write({'state': 'draft', 'audit_uid': False, 'audit_date': False})
        return True
