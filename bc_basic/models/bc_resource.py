﻿# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import Warning
import datetime

class bc_resource(models.Model):
	_name = 'bc.resource'

	name = fields.Char(required=True)
	resourcetype = fields.Many2one('bc.resouretype')
	lngblock = fields.Many2one('bc.enum',domain="[('parent_id','=',1)]", required=True)
	lngfloor = fields.Many2one('bc.enum',domain="[('parent_id','=',2)]", required=True)
	company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id)
	price = fields.Integer()
	state = fields.Selection([('draft',u'草稿'),('audit',u'审核'),('close',u'关闭')])
	remark = fields.Text()
	audit_uid = fields.Many2one('res.users')
	audit_date = fields.Datetime()

	@api.model
	def create(self, vals):
		vals['state'] = 'draft'
		return super(bc_resource,self).create(vals)

	@api.multi
	def unlink(self):
		if self.state in ('audit','close') :
			raise Warning(_(u'房间号{0}的状态是{1},不能删除'.format(self.name,self.state)))
		return super(bc_resource,self).unlink()

	#审核
	@api.model
	def btn_audit(self):
		self.write({'state': 'audit','audit_uid': self.env.user.id,'audit_date': datetime.datetime.utcnow()})
		return True

	#反审核
	@api.model
	def btn_unaudit(self):
		self.write({'state': 'draft','audit_uid': False,'audit_date': False})
		return True
